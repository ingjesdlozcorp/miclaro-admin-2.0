export const environment = {
    production: false,
    API_PATH_TELEMARKETING: 'https://ef757732-b391-4fcc-a4e6-0dfb04040326.mock.pstmn.io/proccess/procesos_mobile.aspx',
    SERVICE_ENTRY: 'https://ef757732-b391-4fcc-a4e6-0dfb04040326.mock.pstmn.io/proccess/procesos_mobile.aspx',
    SERVICE_ENTRY_INSTALLMENT: 'https://ef757732-b391-4fcc-a4e6-0dfb04040326.mock.pstmn.io/proccess/procesos_mobile.aspx',
    MOCK_API: true
};
