export const environment = {
    production: false,
    API_PATH_TELEMARKETING: 'https://telemarketing.claropr.com/proccess/procesos_mobile.aspx',
    SERVICE_ENTRY: 'https://wsebillregister.claropr.com/ebill_register_webapp_war_exploded/ebill/',
    // SERVICE_ENTRY_INSTALLMENT: 'https://miclaroadminuat.claropr.com/proccess/procesos_mobile.aspx',
    SERVICE_ENTRY_INSTALLMENT: 'https://ef757732-b391-4fcc-a4e6-0dfb04040326.mock.pstmn.io/proccess/procesos_mobile.aspx',
    MOCK_API: true
};
