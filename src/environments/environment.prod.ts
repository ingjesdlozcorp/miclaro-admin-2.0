export const environment = {
    production: true,
    API_PATH_TELEMARKETING: 'https://telemarketing.claropr.com/proccess/procesos_mobile.aspx',
    SERVICE_ENTRY: 'https://wsebillregister.claropr.com/ebill_register_webapp_war_exploded/ebill/',
    SERVICE_ENTRY_INSTALLMENT: 'https://miclaroadmin.claropr.com/proccess/procesos_mobile.aspx',
    MOCK_API: false
};
