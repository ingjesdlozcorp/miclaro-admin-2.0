import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { mergeMap, timeout } from 'rxjs/operators';
import { Constants } from '../../utilities/constants';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) {
    }

    installmentListener(request: any) {
        return this.serverListener(environment.SERVICE_ENTRY_INSTALLMENT, request).toPromise();
    }

    entryListener(request: any) {
        if (request.method === 'url/generate') {
            const method = request.method
            delete request.method;
            return this.serverListener(environment.SERVICE_ENTRY + method, request).toPromise();
        }
        return this.serverListener(environment.SERVICE_ENTRY, request).toPromise();
    }

    telemarketingListener(request: any) {
        return this.serverListener(environment.API_PATH_TELEMARKETING, request).toPromise();
    }

    public serverListener(url: string, request: any) {
        if (environment.MOCK_API) {
            url = `${url}/${request.method}`;
        }
        const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
        return this.http.post(url, JSON.stringify(request), {observe: 'response', headers: headers}).pipe(
            timeout(Constants.TIMEOUT),
            mergeMap(res => {
                return this.handlerResponse(res);
            }),
        );
    }

    handlerResponse(response: any): Observable<any> {
        return new Observable(observer => {
            observer.next(response);
            observer.complete();
        });
    }
}
