import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private api: ApiService) { }

    login(username: string, password: string, method: string): Promise<any> {
        const params = {
            username,
            password,
            method
        };
        return this.api.installmentListener(params);
    }
}
