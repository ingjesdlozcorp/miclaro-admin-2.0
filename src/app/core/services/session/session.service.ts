import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
declare let alertify: any;


@Injectable({
  providedIn: 'root'
})
export class SessionService {
    private _credentials: any | null;
    private _max_min_inactive = 15;
    constructor(private router: Router) {
        this.validateTimeSession();
    }



    public getMinutesInSessionI(): number {
        if (this.getTimeLogin() !== undefined) {
            return new Date(new Date().getTime() - this.getTimeLogin().getTime()).getMinutes();
        } else {
            return 0;
        }
    }

    validateTimeSession() {
        const savedCredentials = this.getValue('user');
        console.log('savedCredentials', savedCredentials)
        if (savedCredentials) {
            savedCredentials.timeLogin = new Date();
            this._credentials = savedCredentials;
        }


        setInterval(() => {
            if (this.getMinutesInSessionI() >= this._max_min_inactive) {
                if (this.router.url !== '/login') {
                    alertify.alert(
                        'Sesión Inactiva',
                        'No hemos detectado actividad en los últimos ' +
                        this._max_min_inactive +
                        ' minutos. Por favor inicie nuevamente ingresando su nombre de usuario y contraseña.',
                        () => {
                            this._credentials = null;
                            this.setValue('userName', null);
                            this.setValue('user', null);
                            this.setValue('wsp', null);
                            this.router.navigate(['/'], { replaceUrl: true }).then();
                        }
                    );
                }
            } else if (this.getTimeLogin === undefined || this._credentials === null) {
                this.router.navigate(['/'], { replaceUrl: true }).then();
            }
        }, 60000); // 60.000 milisegundos Referentes a 1 min
    }


    /**
     * Retorna el tiempo en que se identifico
     * @return Object Type Date
     */
    public getTimeLogin(): Date {
        if (this.credentials === null || this.credentials === undefined) {
            return new Date('01/01/1900'); // se manda una fecha expirada cuando no tenga session activa
        } else {
            console.log(this.credentials.timeLogin);
            return this.credentials.timeLogin;
        }
    }

    get credentials(): any | null {
        return this._credentials;
    }


    public getValue = (key: string): any | null => {
        let data: any;
        let dataString = sessionStorage.getItem(key)
        dataString? data = JSON.parse(dataString) : data = dataString;
        return data;
    };

    setValue = (key: string, data: any) => {
        if (data) {
            sessionStorage.setItem(key, JSON.stringify(data));
        } else {
            sessionStorage.removeItem(key);
        }
    };

    public getCredentials(): any | null {
        return this._credentials;
    }

    public setCredentials(credentials?: any) {
        this._credentials = credentials || null;
        this.setValue('user', this._credentials);
    }

    /*
    validaSessionActiva() {
        if (!this.isAuthenticated() || this.getTimeLogin === undefined) {
            this.router.navigateByUrl('/').then();
            return -1;
        }
    }
    */

    /**
     * Checks is the user is authenticated.
     * @return True if the user is authenticated.
     */
    isAuthenticated(): boolean {
        return !!this.credentials;
    }
}
