import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  imgUrl: string =  `/assets/images/foot-logo.png`;
  constructor() { }

  ngOnInit(): void {
  }

}
