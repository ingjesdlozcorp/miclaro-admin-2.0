import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    imgUrl = `/assets/images/pr-flag.png`;

    constructor(private router: Router) { }

    ngOnInit(): void { }

    closeSession() {
        sessionStorage.clear();
        this.router.navigate(['login'], {replaceUrl: true});
    }

    goHome() {
        this.router.navigate(['shop'], {replaceUrl: true});
    }
}
