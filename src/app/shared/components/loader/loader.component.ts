import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {

    constructor() { }

    ngOnInit(): void {
        document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
        document.getElementsByTagName('body')[0].style.cursor = 'progress';
    }

    ngOnDestroy(): void {
         document.getElementsByTagName('body')[0].style.overflowY = 'auto';
         document.getElementsByTagName('body')[0].style.cursor = 'default';
    }


}
