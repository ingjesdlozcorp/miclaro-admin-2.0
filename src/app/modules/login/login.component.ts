import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'src/app/core/services/api/login.service';
import { LoginResponse } from 'src/app/core/interfaces/models';
import {SessionService} from "../../core/services/session/session.service";


declare let alertify: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    imgUrl: string = `/assets/images/`;
    flagUrl: string = this.imgUrl + 'pr-flag.png';
    logoUrl: string = this.imgUrl + 'miclaro-logo.png';
    bannerUrl: string = this.imgUrl + 'log-banner.jpg';
    iconUrl: string = this.imgUrl + 'claro-icon.png';
    title: string = 'Inicio';
    subtitle: string = 'Login';
    rememberLogin: any = localStorage.getItem('rememberLogin');

    loginForm = new FormGroup({
        user: new FormControl('', [Validators.required, Validators.minLength(5)]),
        password: new FormControl('', [Validators.required]),
        remember: new FormControl(false)
    });

    loading = false;

    constructor(private router: Router, private userService: LoginService, private sessionService: SessionService) { }

    ngOnInit(): void {
        if (sessionStorage.getItem('userName') !== '') {
            if (sessionStorage.getItem('user')) {
               // this.router.navigate(['/shop'], {replaceUrl: true});
            }
        }
    }

    activate() {
            if (this.loginForm.value.remember) {
                this.sessionService.setValue('userName', this.loginForm.value.user);
                this.sessionService.setValue('password', this.loginForm.value.password);
                localStorage.setItem('rememberLogin', 'true');
            } else {
                this.sessionService.setValue('userName', null);
                this.sessionService.setValue('password', null);
                localStorage.removeItem('rememberLogin');
            }
            this.rememberLogin = localStorage.getItem('rememberLogin');
    }

    submitForm() {
        if (this.loginForm.value.user === '' || this.loginForm.value.password === '') {
            alertify.alert('Aviso','Debe ingresar su usuario y contraseña.');
            return;
        }
        if (this.rememberLogin) {
            sessionStorage.setItem('userName', this.loginForm.value.user);
        } else {
            sessionStorage.removeItem('userName');
        }
        this.loading = true;
        this.userService.login(this.loginForm.value.user, this.loginForm.value.password, 'loginad').then(
            (response) => {
                this.loading = false;
                let userPermissions = [];
                let codLoginExp = '045';
                console.log(response['body']);
                let data: LoginResponse = response['body'];
                if (!data.hasError) {
                    if (data.code === codLoginExp) {
                        alertify.alert('Aviso','Su contraseña ha expirado, por favor comuníquese con su Administrador');
                    } else {
                        let user = data.object
                        let time = new Date();
                        Object.defineProperty(user, 'timeLogin', {
                            value: time,
                            writable: true,
                            enumerable: true,
                            configurable: true
                        });
                        this.sessionService.setCredentials(user);
                        this.sessionService.setValue('wsp', this.loginForm.value.password);
                        if (this.rememberLogin) {
                            this.loginForm.value.remember = true
                        } else {
                            this.loginForm.value.remember = false
                        }
                        let permissions: any [] = data.object.permissions;
                        // format user permissions
                        permissions.forEach((permission) => {
                            userPermissions.push(permission.id);
                        });

                        if (data.object.roles[0].roleId === 'MCA-VENTAS') {
                           // this.router.navigate(['shop'], {replaceUrl: true});
                        } else {
                            alertify.alert('Aviso','Usuario Inválido');
                        }
                    }

                }

            },
            error => {
                this.loading = false;
                if (error === 'El usuario esta bloqueado.') {
                    alertify.alert('Aviso','Los usuarios con roles Administrativos o de Servicio al Cliente deben comunicarse con el Departamento de Seguridad para reactivar su cuenta.');
                } else {
                    alertify.alert('Aviso','No se ha podido inciar sesi&oacute;n con el usuario y contrase&ntilde;a indicada');
                }
            }
        )
    }

}
